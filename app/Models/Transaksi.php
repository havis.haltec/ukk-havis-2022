<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode_booking',
        'user_id',
        'status',
        'total'
    ];

    public function scopeUserActive($query){
        return $query->where('user_id', auth()->user()->id);
    }

    public function scopeUnpaid($query){
        return $query->where('status', 3);
    }

    public function scopeCheckin($query){
        return $query->where('status', 1);
    }

    public function scopeCheckout($query){
        return $query->where('status', 0);
    }

    public function scopePendapatan($query){
        return $query->where('status', 0)->orWhere('status', 1);
    }

    public function scopeFilter($query, array $filters){
        if($filters['from'] == null && $filters['status'] == null && $filters['to'] == null){
            $query->where('status', 1)->orWhere('status', 0);
        }

        $query->when($filters['from'] ?? false, function($query, $from){
            return $query->whereHas('transaksiDetail', function($query) use($from) {
                $query->where('checkin', '>=' , Carbon::parse($from)->format('Y-m-d'));
            });
        });

        $query->when($filters['to'] ?? false, function($query, $to){
            return $query->whereHas('transaksiDetail', function($query) use ($to){
                $query->where('checkout', '<=' , Carbon::parse($to)->format('Y-m-d'));
            });
        });

        $query->when($filters['status'] ?? false, function($query, $status){
            switch($status){
                case 'checkin':
                    return $query->where(['status' => 1]);
                case 'checkout':
                    return $query->where(['status' => 0]);
            }
        });
    }

    public function transaksiDetail(){
        return $this->hasMany(TransaksiDetail::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $table = 'cart';
    protected $guarded = [];

    public function scopeActiveUser($query){
        return $query->where('user_id', auth()->user()->id);
    }

    public function kamar(){
        return $this->belongsTo(Kamar::class);
    }

}

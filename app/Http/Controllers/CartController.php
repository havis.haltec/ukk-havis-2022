<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\Cart;
use App\Models\Kamar;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            return DataTables::of(Cart::with('kamar', 'kamar.tipeKamar')->activeUser()->get())
                ->editColumn('harga_satuan', function($row){
                    $checkin = Carbon::parse($row->checkin);
                    $checkout = Carbon::parse($row->checkout);
                    $diff = $checkin->diffInDays($checkout);
                    return number_format($row->jumlah * ($row->harga_satuan * $diff));
                })
                ->editColumn('checkin', function($row){
                    return Carbon::parse($row->checkin)->toFormattedDateString();
                })
                ->editColumn('checkout', function($row){
                    return Carbon::parse($row->checkout)->toFormattedDateString();
                })
                ->addColumn('total_hari', function($row){
                    $checkin = Carbon::parse($row->checkin);
                    $checkout = Carbon::parse($row->checkout);
                    $diff = $checkin->diffInDays($checkout);
                    return $diff." hari";
                })
                ->addColumn('_', function($row){
                    return "
                        <button class='btn btn-success' onclick='view(".$row->id.")'>Lihat</button>
                        <button class='btn btn-primary' onclick='edit(".$row->id.")'>Ubah</button>
                        <button class='btn btn-danger'  onclick='destroy(".$row->id.")'>Batal</button>
                    ";
                })
                ->rawColumns(['_'])
                ->make(true);
        }
        return view('pages.cart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request('kamar_id')){
            $model = Kamar::find(request('kamar_id'));
            return view('pages.cart.create', compact('model'));
        }
        return view('pages.cart.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CartRequest $request)
    {   
        $checkin  = Carbon::parse(request('checkin'))->toDateString();
        $checkout = Carbon::parse(request('checkout'))->toDateString();
        $now      = Carbon::now()->toDateString();
        $kamarReq = Kamar::with('tipeKamar')->find(request('kamar_id'));
        $cart = Cart::activeUser()->get();

        if(request('jumlah') > $kamarReq->stok){
            if(request()->ajax()){
                return response()->json([
                    'success' => false,
                    'message' => 'Tidak cukup kamar',
                ]);
            }else{
                return redirect()->route('landing')->with('error_jumlah', 'Tidak cukup kamar');
            }
        }

        if($checkin <= $now || $checkout <= $checkin){
            if(request()->ajax()){
                return response()->json([
                    'success' => false,
                    'message' => 'Reservasi tidak boleh di masa lampau',
                ]);
            }else{
                return redirect()->route('landing')->with('error_jumlah', 'Reservasi tidak boleh di masa lampau');
            }
        }

        if(count($cart) > 0){
            $kamarOld = Kamar::with('tipeKamar')->find($cart[0]->kamar_id);
            if($cart = Cart::activeUser()->where(['kamar_id' => request('kamar_id')])->first()){
                if($cart->update(['jumlah' => $cart->jumlah + intval(request('jumlah'))])){
                    if($kamarReq->update(['stok' => ($kamarReq->stok - intval(request('jumlah')))])){
                        if(request()->ajax()){
                            return response()->json([
                                'success' => true,
                                'message' => 'Keranjang belanja Berhasil ditambahkan',
                                'count'   => Cart::activeUser()->count()
                            ]);
                        }
                        return redirect()->route('landing')->with('success', 'Keranjang belanja Berhasil ditambahkan');
                    }
                }
            }else{
                if($kamarOld->tipeKamar->name == $kamarReq->tipeKamar->name){
                    $req = [
                        'user_id' => auth()->user()->id,
                        'kamar_id'      => request('kamar_id'),
                        'checkin'  => Carbon::parse(request('checkin'))->format('Y-m-d'),
                        'checkout' => Carbon::parse(request('checkout'))->format('Y-m-d'),
                        'jumlah'     => request('jumlah'),
                        'harga_satuan' => $kamarReq->harga,
                        'nama_tamu'  => request('nama_tamu'),
                        'no_hp'      => request('no_hp'),
                    ];
                    if(Cart::create($req)){
                        if($kamarReq->update(['stok' => ($kamarReq->stok - intval(request('jumlah')))])){
                            if(request()->ajax()){
                                return response()->json([
                                    'success' => true,
                                    'message' => 'Keranjang belanja Berhasil ditambahkan',
                                    'count'   => Cart::activeUser()->count()
                                ]);
                            }
                            return redirect()->route('landing');
                        }
                    }
                } else {
                    if(request()->ajax()){
                        return response()->json([
                            'success' => false,
                            'message' => 'Tipe kamar harus sama dalam pemesanan berikutnya',
                            'count'   => Cart::activeUser()->count()
                        ]);
                    }
                    return redirect()->route('landing')->with('error_jumlah', 'Tipe kamar harus sama dalam pemesanan berikutnya');
                }
            }
        } else {
            $req = [
                'user_id' => auth()->user()->id,
                'kamar_id'    => request('kamar_id'),
                'checkin'  => Carbon::parse(request('checkin'))->format('Y-m-d'),
                'checkout' => Carbon::parse(request('checkout'))->format('Y-m-d'),
                'jumlah'   => request('jumlah'),
                'harga_satuan' => $kamarReq->harga,
                'nama_tamu' => request('nama_tamu'),
                'no_hp'     => request('no_hp'),
            ];

            if(Cart::create($req)){
                if($kamarReq->update(['stok' => ($kamarReq->stok - intval(request('jumlah')))])){
                    if(request()->ajax()){
                        return response()->json([
                            'success' => true,
                            'message' => 'Keranjang belanja Berhasil ditambahkan',
                            'count'   => Cart::activeUser()->count()
                        ]);
                    }
                    return redirect()->route('landing');
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Cart::with('kamar.tipeKamar')->activeUser()->find($id);
        return view('pages.cart.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Cart::with('kamar.tipeKamar')->activeUser()->find($id);
        $model->checkin = Carbon::parse($model->checkin)->format('m/d/Y');
        $model->checkout = Carbon::parse($model->checkout)->format('m/d/Y');
        return view('pages.cart.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CartRequest $request, $id)
    {
        $checkin  = Carbon::parse(request('checkin'))->toDateString();
        $checkout = Carbon::parse(request('checkout'))->toDateString();
        $now      = Carbon::now()->toDateString();
        $cart = Cart::activeUser()->find($id);
        $kamarReq = Kamar::with('tipeKamar')->find($cart->kamar_id);

        if($checkin <= $now || $checkout <= $checkin){
            return response()->json([
                'success' => false,
                'message' => 'Reservasi tidak boleh di masa lampau',
            ]);
        }

        if(request('jumlah') > $kamarReq->stok){
            if(request()->ajax()){
                return response()->json([
                    'success' => false,
                    'message' => 'Kamar tersedia tidak cukup',
                ]);
            }else{
                return redirect()->route('landing')->with('error_jumlah', 'Kamar tersedia tidak cukup');
            }
        }

        $req = [
            'user_id' => request('user_id'),
            'kamar_id'    => request('kamar_id'),
            'checkin'  => Carbon::parse(request('checkin'))->format('Y-m-d'),
            'checkout' => Carbon::parse(request('checkout'))->format('Y-m-d'),
            'jumlah'   => request('jumlah'),
            'harga_satuan' => $kamarReq->harga,
            'nama_tamu' => request('nama_tamu'),
            'no_hp'     => request('no_hp'),
        ];

        // penyesuaian stok
        if(request('jumlah') > $cart->jumlah){
            $stok = $kamarReq->stok - (intval(request('jumlah')) - $cart->jumlah);
        }else{
            $stok = $kamarReq->stok + ($cart->jumlah - intval(request('jumlah')));
        }

        if($cart->update($req)){
            if($kamarReq->update(['stok' => $stok])){
                return response()->json([
                    'success' => true,
                    'message' => 'Keranjang belanja berhasil diperbarui',
                    'count'   => Cart::activeUser()->count()
                ]);
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Cart::activeUser()->find($id);
        $kamar = Kamar::find($model->kamar_id);
        if($kamar->update(['stok' => $kamar->stok + $model->jumlah])){
            if($model->delete()){
                return response()->json([
                    'success' => true,
                    'message' => 'Pemesanan kamar berhasil dibatalkan',
                    'count'   => Cart::activeUser()->count()
                ]);
            }
        }
    }

    public function destroyAll(){
        if(Cart::activeUser()->count() > 0){
            $model = Cart::activeUser()->get();
            DB::beginTransaction();
            try{
                foreach($model as $row){
                    $kamar = Kamar::find($row->kamar_id);
                    $kamar->update(['stok' => $kamar->stok + $row->jumlah]);
                }
                Cart::activeUser()->delete();
                DB::commit();
                return response()->json([
                    'status' => true,
                    'message' => 'Pemesanan kamar berhasil dibatalkan',
                    'count'   => Cart::activeUser()->count()
                ]);
            }catch(Exception $e){
                DB::rollBack();
                return response()->json([
                    'status' => false,
                    'message' => 'Pemesanan kamar gagal dibatalkan',
                    'count'   => Cart::activeUser()->count()
                ]);
            }
        }
    }
}

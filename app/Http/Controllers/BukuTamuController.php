<?php

namespace App\Http\Controllers;

use App\Models\Kamar;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class BukuTamuController extends Controller
{
    public function index(){
        if(request()->ajax()){
            if(request('scope') == 'history'){
                $model = Transaksi::with('user', 'transaksiDetail')->whereIn('status', [0, 2]);
            }else{
                $model = Transaksi::with('user', 'transaksiDetail')->whereIn('status', [1, 3]);
            }

            if(request('from')){
                $model->whereHas('transaksiDetail', function($query){
                    $query->where('checkin', '>=' , Carbon::parse(request('from'))->format('Y-m-d'));
                });
            }
            if(request('to')){
                $model->whereHas('transaksiDetail', function($query){
                    $query->where('checkout', '<=', Carbon::parse(request('to'))->format('Y-m-d'));
                });
            }
            return DataTables::of($model->get())
                ->editColumn('total', function($row){
                    return number_format($row->total);
                })
                ->editColumn('pemesan', function($row){
                    return $row->user->name;
                })
                ->addColumn('status', function($row){
                    switch($row->status){
                        case 1: 
                            return "
                                    <button class='btn btn-secondary btn-sm' onclick='update(`checkout`, ".$row->id.")'>Check Out</button>
                                    <button class='btn btn-success btn-sm' onclick='update(`checkin`, ".$row->id.")'>Check In</button>
                                ";
                        case 0:
                            return "<button class='btn btn-danger btn-sm'>Checkout</button>";
                        case 2:
                            return "<button class='btn btn-danger btn-sm'>Dibatalkan</button>";
                        default:
                            return "
                                <button class='btn btn-secondary btn-sm' onclick='update(`checkin`, ".$row->id.")'>Checkin</button>
                                <button class='btn btn-warning   btn-sm' >Unpaid</button>
                                <button class='btn btn-secondary btn-sm' onclick='update(`cancel`, ".$row->id.")'>Batal Pemesanan</button>
                            ";
                    }
                })
                ->addColumn('_', function($row){
                    return "<button class='btn btn-info' onclick='show(".$row->id.")'>Detail</button>";
                })
                ->rawColumns(['status', '_'])
                ->make(true);
        }
        return view('pages.buku_tamu.index');
    }

    public function show($id){
        $model = Transaksi::with('transaksiDetail')->find($id);
        return view('pages.buku_tamu.show', compact('model'));
    }

    public function update($id){
        $model = Transaksi::find($id);
        switch(request('type')){
            case 'checkin':
                $model->update(['status' => 1]);
                foreach($model->transaksiDetail as $row){
                    $detailTransaksi = TransaksiDetail::find($row->id);
                    $detailTransaksi->update(['status' => 1]);
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Transaksi berhasil dicheckin'
                ]);
            case 'checkout':
                $model->update(['status' => 0]);
                foreach($model->transaksiDetail as $row){
                    $detailTransaksi = TransaksiDetail::with('kamar')->find($row->id);
                    $kamar = Kamar::find($detailTransaksi->kamar->id);
                    $stok  = $kamar->stok + intval($detailTransaksi->jumlah);
                    $kamar->update(['stok' => $stok]);
                    $detailTransaksi->update(['status' => 0]);
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Transaksi berhasil dicheckout'
                ]);
            case 'cancel':
                $model->update(['status' => 2]);
                foreach($model->transaksiDetail as $row){
                    $detailTransaksi = TransaksiDetail::with('kamar')->find($row->id);
                    $kamar = Kamar::find($detailTransaksi->kamar->id);
                    $stok  = $kamar->stok + intval($detailTransaksi->jumlah);
                    $kamar->update(['stok' => $stok]);
                    $detailTransaksi->update(['status' => 2]);
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Transaksi berhasil dibatalkan'
                ]);
            default :
                $model->update(['status' => 3]);
                foreach($model->transaksiDetail as $row){
                    $detailTransaksi = TransaksiDetail::find($row->id);
                    $detailTransaksi->update(['status' => 3]);
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Transaksi berhasil diubah'
                ]);
        }
    }
}

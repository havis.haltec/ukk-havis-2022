<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Yajra\DataTables\Facades\DataTables;

class PendapatanController extends Controller
{
    public function index(){
        $nominalPendapatan = 0;
        $model = Transaksi::with('user', 'transaksiDetail')->pendapatan()->get();
        if($req = request(['from', 'to', 'status'])){
            $model = Transaksi::with('user', 'transaksiDetail')->filter($req)->get();
        }
        if(request()->ajax()){
            return DataTables::of($model)
                ->editColumn('total', function($row){
                    return number_format($row->total);
                })
                ->editColumn('status', function($row){
                    switch($row->status){
                        case 0:
                            return '<button class="btn btn-sm btn-danger">Checkout</button>';
                        case 1:
                            return '<button class="btn btn-sm btn-success">Checkin</button>';
                    }
                })
                ->addColumn('_', function($row){
                    return "<button class='btn btn-info btn-sm' onclick='show(".$row->id.")'>Detail</button>";
                })
                ->rawColumns(['_', 'status'])
                ->make(true);
        }
        foreach($model as $row){
            $nominalPendapatan += $row->total;
        }
        return view('pages.pendapatan.index', compact('nominalPendapatan'));
    }

    public function getTotalPendapatan(){
        if($req = request(['from', 'to', 'status'])){
            $model = Transaksi::with('user', 'transaksiDetail')->filter($req)->get();
            $nominalPendapatan = 0;
            foreach($model as $row){
                $nominalPendapatan += $row->total;
            }
            return response()->json([
                'status' => true,
                'data' => number_format($nominalPendapatan)
            ]);
        }
    }
}

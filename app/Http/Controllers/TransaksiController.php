<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Kamar;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use Barryvdh\DomPDF\Facade\Pdf as PdfFacade;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Spatie\PdfToImage\Pdf;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            return DataTables::of(Transaksi::userActive()->get())
            ->editColumn('_', function($row){
                return "
                    <button class='btn btn-info fw-bold text-white btn-sm' onclick='print_invoice(".$row->id.")'>Cetak</button>
                    <button class='btn btn-success fw-bold text-white btn-sm' onclick='show(".$row->id.")'>Lihat</button>
                ";
            })
            ->editColumn('status', function($row){
                switch($row->status){
                    case 0:
                        return "Checkout";
                    case 1:
                        return "Active";
                    case 2: 
                        return "Dibatalkan";
                    default:
                        return "Unpaid";
                }
            })
            ->rawColumns(['status', '_'])
            ->make(true);
        }
        return view('pages.transaksi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = Cart::activeUser()->get();

        if($cart->count() > 0){

            DB::beginTransaction();
            try{
                $transaksi   = Transaksi::where([
                    'status'  => 3,
                    'user_id' => auth()->user()->id 
                ])->first();

                if($transaksi) {
                    return response()->json([
                        'status' => false,
                        'message' => 'Selesaikan transaksi terlebih dahulu.'
                    ]);
                }

                $transaksiId = Transaksi::create([
                    'kode_booking' => $this->getTransactionCode(),
                    'user_id'      => auth()->user()->id,
                    'total'        => 0,
                    'status'       => 3
                ]);
    
                $recordTransaksiDetail = [];
                $totalHarga = 0;

                foreach($cart as $index => $value){
                    $kamar = Kamar::find($value['kamar_id']);
                    $checkin = Carbon::parse($value['checkin']);
                    $checkout = Carbon::parse($value['checkout']);
                    $diff = $checkin->diffInDays($checkout);
                    $totalHarga += ($value['jumlah'] * ( $kamar->harga * $diff));

                    for($i = 0; $i < $value['jumlah']; $i++){
                        $recordTransaksiDetail[$i+$index]['transaksi_id'] = $transaksiId->id;
                        $recordTransaksiDetail[$i+$index]['kode_ruang'] = $this->getRoomCode($kamar, $i);
                        $recordTransaksiDetail[$i+$index]['kamar_id'] = intval($value['kamar_id']);
                        $recordTransaksiDetail[$i+$index]['jumlah'] = intval($value['jumlah']);
                        $recordTransaksiDetail[$i+$index]['checkin'] = $value['checkin'];
                        $recordTransaksiDetail[$i+$index]['checkout'] = $value['checkout'];
                        $recordTransaksiDetail[$i+$index]['harga_satuan'] = $kamar->harga;
                        $recordTransaksiDetail[$i+$index]['tamu'] = $value['nama_tamu'];
                        $recordTransaksiDetail[$i+$index]['no_hp'] = $value['no_hp'];
                        $recordTransaksiDetail[$i+$index]['status'] = 3;
                    }
                }

                $transaksiId->update(['total' => $totalHarga]);
                TransaksiDetail::insert($recordTransaksiDetail);
                Cart::activeUser()->delete();
                DB::commit();
                return response()->json([
                    'status' => true,
                    'message' => 'Transaksi berhasil dibuat'
                ]);
    
            }catch(Exception $e){
                dd($e);
                DB::rollBack();
                return response()->json([
                    'status' => false,
                    'message' => 'Transaksi gagal dibuat'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $model = Transaksi::with('transaksiDetail.kamar.tipeKamar')
            ->where('id', $id)
            ->where('user_id', auth()->user()->id)
            ->first();
        return view('pages.report.invoice', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        //
    }

    public function invoiceImg($id){
        $model = Transaksi::with('transaksiDetail.kamar.tipeKamar')
            ->where('id', $id)
            ->where('user_id', auth()->user()->id)
            ->first();

        $pdf = PdfFacade::loadView('pages.report.invoice', compact('model'));
        $content = $pdf->download()->getOriginalContent();
        $pathName = 'public/pdf/'.rand(0,999999999).'.pdf';
        Storage::put($pathName, $content);
        $pdfFile = Storage::get($pathName);
        
        $pdf = new Pdf($pdfFile);
        $pdf->saveImage(asset('public/image'));
    }

    public function invoice($id){
        $model = Transaksi::with('transaksiDetail.kamar.tipeKamar')
            ->where('id', $id)
            ->where('user_id', auth()->user()->id)
            ->first();
        $pdf = PdfFacade::loadView('pages.report.invoice', compact('model'));
        return $pdf->download();
    }

    protected function getTransactionCode(){
        $code ="BOOK-".Transaksi::count()+1;
        return $code;
    }

    protected function getRoomCode($roomModel, $inc){
        $model = TransaksiDetail::with('kamar', 'transaksi')->whereHas('transaksi', function($query){
            $query->where('status', 1);
        })->orderBy('kode_ruang', 'desc')->where('kamar_id', $roomModel->id)->get();

        if(!$model->isEmpty()){
            $roomName = explode('#', $model[0]->kode_ruang)[0];
            $kode = [];
            foreach($model as $row){
                $kode[] = intval(explode('#', $row->kode_ruang)[1]);
            }

            $count = array_diff($this->numberRange($roomModel->jumlah), $kode);
            $numberCode = (min($count)+1);
            // jika jumlah pesanan lebih dari 1
            if($inc > 0){
                // jika kode ruang lebih dari jumlah ruang
                if($numberCode > $roomModel->jumlah){
                    return $roomName.'#'.max($count);
                }
                return $roomName.'#'.$numberCode;
            }
            return $roomName.'#'.min($count);
        }
        return $roomModel->name.'#'.$inc+1;
    }

    protected function numberRange($number){
        $arr = [];
        for($i = 1; $i <= $number; $i++){
            $arr[] = $i;
        }
        return $arr;
    }
}

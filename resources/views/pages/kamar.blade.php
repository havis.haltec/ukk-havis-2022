<x-mainLayout>
    <x-carrousel>
        <div class="carousel-inner">
            @if($model)
                @foreach($model as $key => $value)
                    <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                        <img src="{{ asset('storage/'.$value['banner']) }}" class="d-block w-100 img-fit">
                    </div>
                @endforeach
            @endif
        </div>
    </x-carrousel>
    <div class="mt-5">
        <div class="row">
            <h2>Kamar</h2>
        </div>
    </div>
    <div class="row flex-wrap mb-5 justify-content-between mt-5">
        @if($model)
            @foreach($model as $key => $value)
                <div class="col-md-3 mb-5">
                    <div class="card h-100">
                        <img src="{{ asset('storage/'.$value['banner']) }}" height="200" class="card-img-top img-fit-cover" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $value['name'] }}</h5>
                            <p class="card-text"><span class="fw-bold">Jumlah tersedia: </span> {{ $value['stok'] }} kamar</p>
                            <p class="card-text"><span class="fw-bold">Tipe Kamar: </span> {{ $value['tipeKamar']->name }}</p>
                            <p class="card-text"><span class="fw-bold">Harga: </span>Rp.{{ number_format($value['harga']) }} / hari</p>
                        </div>
                        <div class="card-footer d-flex justify-content-between">
                            <a href="{{ route('kamar.hotel.show', $value['id']) }}" class="btn btn-outline-warning fw-bold">Lihat Kamar</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    @slot('script')
        <script>
            function store(){
                $('#formCreate .alert').remove();
                $.ajax({
                    url: '{{ route('cart.store') }}',
                    dataType: 'json',
                    method: 'post',
                    data: $('#formCreate').serialize(),
                    success: (response) => {
                        if(response.success){
                            Swal.fire({
                                title: 'success',
                                icon: 'success',
                                text: response.message
                            });
                        }else{
                            Swal.fire({
                                title: 'error',
                                icon: 'error',
                                text: response.message
                            });
                        }
                        $('#count').html(response.count)
                        bootbox.hideAll();
                    },
                    error: (err) => {
                        let response = JSON.parse(err.responseText);
                        $('#formCreate').prepend(validation(response));
                    }
                });
            }

            function validation(errors){
                var validations = '<div class="alert alert-danger">';
                $.each(errors.errors, function(i, error){
                    validations += error[0]+'<br>';
                });
                validations += '</div>';
                return validations;
            }
        </script>
    @endslot
</x-mainLayout>
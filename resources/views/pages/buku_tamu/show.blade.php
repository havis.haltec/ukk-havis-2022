<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title>Invoice Transaksi</title>

		<!-- Favicon -->
		<link rel="icon" href="./images/favicon.png" type="image/x-icon" />

		<!-- Invoice styling -->
		<style>

            #customers {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: center;
                background-color: #626464;
                color: white;
            }
		</style>
	</head>
	<body>
        <table id="customers">
            <tr>
                <th width="25%">Nama Kamar</th>
                <th>Tipe Kamar</th>
                <th width="25%">Kode Ruang</th>
                <th width="25%">Check In</th>
                <th width="25%">Check Out</th>
                <th>Nama Tamu</th>
                <th>No Hp</th>
                <th>Status</th>
            </tr>
            @foreach ($model->transaksiDetail as $item)
                <tr>
                    <td width="25%">{{ $item->kamar->name }}</td>
                    <td>{{ $item->kamar->tipeKamar->name }}</td>
                    <td width="25%">{{ $item->kode_ruang }}</td>
                    <td width="25%">{{ \Carbon\Carbon::parse($item->checkin)->toFormattedDateString() }}</td>
                    <td width="25%">{{ \Carbon\Carbon::parse($item->checkout)->toFormattedDateString() }}</td>
                    <td>{{ $item->tamu }}</td>
                    <td>{{ $item->no_hp }}</td>
                    <td>
                        @switch($model->status)
                            @case(0)
                                Check Out
                                @break
                            @case(1)
                                Active
                                @break
                            @default
                                Unpaid
                                @break
                        @endswitch
                    </td>
                </tr>
            @endforeach
        </table>
	</body>
</html>

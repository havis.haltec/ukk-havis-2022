<script>
    var table;
    var tableHistory;
    $(() => {
        table = $('#table').DataTable({
            ajax:'',
            serverSide: true,
            processing: true,
            responsive: true,
            columns: [
                { data: 'kode_booking', name: 'transaksis.kode_booking' },
                { data: 'pemesan' },
                { data: 'total', name: 'transaksis.total' },
                { data: 'status' },
                { data: '_', searchable: false, sorting: false},
            ]
        });
        tableHistory = $('#table-history').DataTable({
            ajax:'{{ route('buku.tamu.index') }}?scope=history',
            serverSide: true,
            processing: true,
            responsive: true,
            columns: [
                { data: 'kode_booking', name: 'transaksis.kode_booking' },
                { data: 'pemesan' },
                { data: 'total', name: 'transaksis.total' },
                { data: 'status' },
                { data: '_', searchable: false, sorting: false},
            ]
        });
        $('#from').datepicker();
        $('#to').datepicker();
    });

    function update(type, id){
        let url = "{{ route('buku.tamu.update', ':id') }}";
        url = url.replace(':id', id);
        
        $.ajax({
            url,
            method: 'post',
            data: {'_method': 'put', '_token': '{{ csrf_token() }}', 'type' : type},
            success: (response) => {
                if(response.status){
                    Swal.fire({
                        icon: 'success',
                        title: response.message
                    });
                    table.ajax.reload()
                    tableHistory.ajax.reload()
                }else {
                    Swal.fire({
                        icon: 'error',
                        title: response.message
                    });
                }
            }
        });
    }

    function show(id){
        let url = `{{ route('buku.tamu.show',':id') }}`;
        url = url.replace(':id', id);
        $.ajax({
            url,
            success:(response) => {
                bootbox.dialog({
                    title: 'Detail aksi',
                    size: 'large',
                    message: response
                });
            }
        });
    }

    function reset_filter(){
        $('#from').val('');
        $('#to').val('');
        let filter = $('#filter').serialize();
        table.ajax.url(`{{ route('buku.tamu.index') }}?${filter}`).load();
        tableHistory.ajax.url(`{{ route('buku.tamu.index') }}?${filter}&scope=history`).load();
    }

    function filter(){
        let filter = $('#filter').serialize();
        table.ajax.url(`{{ route('buku.tamu.index') }}?${filter}`).load();
        tableHistory.ajax.url(`{{ route('buku.tamu.index') }}?${filter}&scope=history`).load();
    }
</script>
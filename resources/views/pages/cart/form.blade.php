<div class="row">
    @if($model->kamar_id)
        {!! Form::hidden('_method', 'PUT'); !!}
    @endif
    <div class="col-md-6">
        <div class="form-group">
            <label for="check-in" class="fw-bold mb-2">Check In </label>
            {!! Form::text('checkin', $model['checkin'] ?? null, ['class' => 'form-control', 'id' => 'check-in', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="check-out" class="fw-bold mb-2">Check Out </label>
            {!! Form::text('checkout', $model['checkout'] ?? null, ['class' => 'form-control', 'id' => 'check-out', 'autocomplete' => 'off']) !!}
        </div>
    </div>
</div>
<div class="row mt-4">
    @if(!$model->tipe_kamar_id)
        <div class="col-md-5">
            <div class="form-group">
                <label for="jumlah" class="fw-bold mb-2">Jumlah</label>
                {!! Form::number('jumlah', $model['jumlah'] ?? null, ['class' => 'form-control', 'id' => 'jumlah', 'min' => '0']) !!}
            </div>
        </div>
    @else
        <div class="col-md-5">
            <div class="form-group">
                <label for="jumlah" class="fw-bold mb-2">Jumlah</label>
                {!! Form::number('jumlah', null, ['class' => 'form-control', 'id' => 'jumlah', 'min' => '0']) !!}
            </div>
        </div>
    @endif
    @if(!$model->tipe_kamar_id)
        <div class="col-md-7">
            <div class="form-group">
                <label for="kamar" class="fw-bold mb-2">Kamar </label>
                {!! Form::hidden('kamar_id', $model->kamar_id) !!}
                {!! Form::text('kamar', $model->kamar->name ?? null , ['class' => 'form-control', 'id' => 'kamar', 'disabled']) !!}
            </div>
        </div>
    @else
        <div class="col-md-7">
            <div class="form-group">
                <label for="kamar" class="fw-bold mb-2">Kamar </label>
                {!! Form::hidden('kamar_id', $model->id) !!}
                {!! Form::text('kamar', $model->name , ['class' => 'form-control', 'id' => 'kamar', 'disabled']) !!}
            </div>
        </div>
    @endif
</div>
<div class="row mt-4">
    <div class="col-md-5">
        <div class="form-group">
            <label for="pemesan" class="fw-bold mb-2">Nama Pemesan</label>
            {!! Form::hidden('user_id',auth()->user()->id ?? null, ['class' => 'form-control', 'id' => 'user_id', 'readonly']) !!}
            {!! Form::text('pemesan',auth()->user()->name ?? null, ['class' => 'form-control', 'id' => 'pemesan', 'readonly']) !!}
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label for="email" class="fw-bold mb-2">Email</label>
            {!! Form::text('email', auth()->user()->email ?? null, ['class' => 'form-control', 'id' => 'email', 'readonly']) !!}
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-md-5">
        <div class="form-group">
            <label for="guest" class="fw-bold mb-2">Nama Tamu</label>
            {!! Form::text('nama_tamu', $model['nama_tamu'] ?? null, ['class' => 'form-control', 'id' => 'guest']) !!}
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label for="no_hp" class="fw-bold mb-2">No Hp</label>
            {!! Form::number('no_hp', $model['no_hp'] ?? null, ['class' => 'form-control', 'id' => 'no_hp']) !!}
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="form-group fw-bold">
        {{-- <label for="harga_satuan" class="fw-bold mb-2">Harga Satuan</label> --}}
        {!! Form::hidden('harga_satuan', $model->harga ?? $model['harga_satuan'], ['class' => 'form-control text-right', 'id' => 'harga_satuan', 'readonly']) !!}
    </div>
</div>
<x-mainLayout>
    <div style="height: 100vh!important" class="m-auto">
        <div class="card mb-3">
            <div class="card-header">
                <h5 class="m-0">Daftar Booking</h5>
            </div>
            <div class="card-body">
                <table id="table" class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Nama Kamar</th>
                            <th>Tipe</th>
                            <th>Jumlah Pemesanan</th>
                            <th class="text-right">Harga</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Total Menginap</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="d-flex justify-content-end m-3">
                @if(App\Models\Cart::activeUser()->count() > 0)
                    <button class="btn btn-success fw-bold" onclick="order()">Pesan</button>
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Transaksi</h5>
            </div>
            <div class="card-body">
                <table id="table-transaksi" class="table table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th>Kode Transaksi</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    @slot('script')
        @include('pages.cart.form_script')
    @endslot
</x-mainLayout>
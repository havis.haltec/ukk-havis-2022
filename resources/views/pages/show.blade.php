<x-mainLayout>
    <div class="row mb-5 justify-content-between">
        <div class="col-md-7 bg-white shadow p-0 rounded">
            <img src="{{ asset('storage/'.$model['banner']) }}" class="w-100 img-fit-cover rounded-top" style="height:30%!important" alt="">
            <div class="p-3 mt-5">
                <h3 class="fw-bold mb-4">Fasilitas Kamar</h3>
                <div class="row flex-wrap">
                    @if($model->fasilitases)
                        @foreach($model->fasilitases as $key => $value)
                            <div class="col-md-6 mb-4">
                                <div class="card h-100">
                                <img src="{{ asset('storage/'.$value->fasilitas->picture) }}" height="200" class="card-img-top img-fit-cover" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $value->fasilitas->name }}</h5>
                                        <p class="card-text">{{ $value->fasilitas->desc }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4 bg-white shadow h-50 rounded">
            <div class="container">
                <h2 class="fw-bold m-0 py-3 text-secondary">{{ $model['name'] }}</h2>
                <div class="row">
                    <div class="col-md-5">
                        <p class="fw-bold text-secondary">Tipe Kamar : </p>
                    </div>
                    <div class="col-md-7">
                        <p> {{ $model->tipeKamar->name }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <p class="fw-bold text-secondary">Jumlah Kamar : </p>
                    </div>
                    <div class="col-md-7">
                        <p>{{ $model['jumlah'] }} Kamar</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <p class="fw-bold text-secondary">Kamar Tersedia : </p>
                    </div>
                    <div class="col-md-7">
                        <p>{{ $model['stok'] }} Kamar</p>
                    </div>
                </div>
                <p class="fw-bold m-0 text-secondary pb-2">Harga</p>
                <div class="row p-0">
                    <h3 class="fw-bold text-danger m-0 pb-3">Rp.{{ number_format($model['harga']) }} <small class="text-muted">/ hari</small></h3>
                </div>
                {!! Form::open(['id' => 'formCreate']) !!}
                    @include('pages.cart.form')
                @if(isset(auth()->user()->id))
                    <div class="text-end mr-4">
                        <button class="btn btn-success fw-bold my-3 w-50" type="button" onclick="store()">Pesan Kamar</button>
                    </div>
                @else
                    <div class="text-end mr-4">
                        <a href="{{ route('login') }}" class="btn btn-success my-3 w-50">Pesan Kamar</a>
                    </div>
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @slot('script')
        <script>
            $('#check-in').datepicker();
            $('#check-out').datepicker();
            function store(){
                $('#formCreate .alert').remove();
                $.ajax({
                    url: '{{ route('cart.store') }}',
                    dataType: 'json',
                    method: 'post',
                    data: $('#formCreate').serialize(),
                    success: (response) => {
                        if(response.success){
                            Swal.fire({
                                title: 'success',
                                icon: 'success',
                                text: response.message
                            });
                        }else{
                            Swal.fire({
                                title: 'error',
                                icon: 'error',
                                text: response.message
                            });
                        }
                        location.reload();
                        $('#count').html(response.count);
                        bootbox.hideAll();
                    },
                    error: (err) => {
                        let response = JSON.parse(err.responseText);
                        $('#formCreate').prepend(validation(response));
                    }
                });
            }

            function validation(errors){
                var validations = '<div class="alert alert-danger">';
                $.each(errors.errors, function(i, error){
                    validations += error[0]+'<br>';
                });
                validations += '</div>';
                return validations;
            }

        </script>
    @endslot
</x-mainLayout>
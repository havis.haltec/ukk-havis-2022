<x-mainLayout>
    <x-carrousel>
        <div class="carousel-inner">
            @if($model)
                @foreach($model as $key => $value)
                    <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                        <img src="{{ asset('storage/'.$value['picture']) }}" class="d-block w-100 img-fit">
                    </div>
                @endforeach
            @endif
        </div>
    </x-carrousel>
    <div class="mt-5">
        <div class="row">
            <h2>Fasilitas Hotel</h2>
        </div>
    </div>
    <div class="row flex-wrap mb-5 mt-5 justify-content-between">
        @if($model)
            @foreach($model as $key => $value)
                <div class="col-md-3 mb-5">
                    <div class="card h-100">
                        <img src="{{ asset('storage/'.$value['picture']) }}" height="200" class="card-img-top img-fit-cover" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $value['name'] }}</h5>
                            <p class="card-text">{{ $value['desc'] }}.</p>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</x-mainLayout>
<x-adminLayout pageName="Pendapatan">
    <div>
        <div>
            <h6>Nominal Pendapatan</h6>
        </div>
        <div>
            <h5 id="nominal-pendapatan-label">{{ number_format($nominalPendapatan) }}</h5>
        </div>
    </div>
    {!! Form::open(['id' => 'filter']) !!}
        <div class="d-flex mb-3 mt-5">
            {!! Form::text('from', null, ['class' => 'form-control w-25', 'id' => 'from', 'placeholder' => 'from', 'autocomplete' => 'off']) !!}
            {!! Form::text('to', null, ['class' => 'form-control w-25 ml-4 mr-4', 'id' => 'to', 'placeholder' => 'to', 'autocomplete' => 'off']) !!}
            {!! Form::select('status', ['' => '-- Pilih Status --', 'checkin' => 'checkin', 'checkout' => 'checkout'], null, ['class' => 'form-control w-25 mr-3', 'id' => 'status']) !!}
            <button type="button" class="btn btn-primary fw-bold m-0" onclick="filter()">Filter</button>
            <button type="button" class="btn btn-secondary fw-bold ml-3" onclick="reset_filter()">Reset</button>
        </div>
    {!! Form::close() !!}
    <br>
    <h5>Rincian Pendapatan</h5>
    <br>
    <table class="table table-bordered table-condensed" id="table">
        <thead>
            <tr>
                <th>Kode Booking</th>
                <th>Pemesan</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    @slot('script')
        @include('pages.pendapatan.form_script')
    @endslot
</x-adminLayout>
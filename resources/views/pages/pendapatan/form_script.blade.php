<script>
    var table;
    $(() => {
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '',
            columns: [
                {data: 'kode_booking', name: 'transaksis.kode_booking'},
                {data: 'user.name', name: 'transaksis.user.name'},
                {data: 'total', name: 'transaksis.total'},
                {data: 'status', name: 'transaksis.status'},
                {data: '_', searchable: false}
            ]
        });
        $('#from').datepicker()
        $('#to').datepicker()
    })

    function show(id){
        let url = `{{ route('buku.tamu.show',':id') }}`;
        url = url.replace(':id', id);
        $.ajax({
            url,
            success:(response) => {
                bootbox.dialog({
                    title: 'Detail aksi',
                    size: 'large',
                    message: response
                });
            }
        });
    }

    function reset_filter(){
        $('#from').val('');
        $('#to').val('');
        $('#status').val('');
        let filter = $('#filter').serialize();
        table.ajax.url(`{{ route('pendapatan.index') }}?${filter}`).load();
        $.ajax({
            url: `{{ route('pendapatan.total') }}?${filter}`,
            method: 'post',
            data: {'_token': '{{ csrf_token() }}'},
            success: (response) =>{
                if(response.status){
                    $('#nominal-pendapatan-label').html(response.data);
                }
            }
        })
    }

    function filter(){
        let filter = $('#filter').serialize();
        table.ajax.url(`{{ route('pendapatan.index') }}?${filter}`).load();

        $.ajax({
            url: `{{ route('pendapatan.total') }}?${filter}`,
            method: 'post',
            data: {'_token': '{{ csrf_token() }}'},
            success: (response) =>{
                if(response.status){
                    $('#nominal-pendapatan-label').html(response.data);
                }
            }
        })
    }
</script>
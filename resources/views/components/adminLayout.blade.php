<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $title ?? env('APP_NAME') }}</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('utils/font-awsome-6/css/all.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('dist/css/overlay-scrollbar.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('utils/datatable/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('utils/datatable/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('utils/sweetalert2/dist/sweetalert2.css') }}">
  <link rel="stylesheet" href="{{ asset('utils/datepicker/datepicker.css') }}">
  <link rel="icon" href="{{ asset('favicon/favicon.ico') }}" type="image/x-icon" />
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light p-4 flex-row-reverse">
    <!-- Left navbar links -->
      <h5>Welcome, {{ ucwords(auth()->user()->name) }} - {{ ucwords(auth()->user()->role) }}</h5>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar main-sidebar-custom sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <span class="brand-text font-weight-light">Hotel Hebat</span>
    </a>

    <x-sidebar></x-sidebar>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ ucwords($pageName) }}</h1>
          </div>
        </div>
      </div> --}}
      <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-header">
            <h4 class="m-0">{{ ucwords($pageName) }}</h4>
          </div>
          <div class="card-body">
            {{ $slot }}
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2022 <a href="#">{{ env('APP_NAME') }}</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('utils/jquery/jquery-3.6.0.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('utils/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('dist/js/overlay-scrollbar.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('utils/datatable/datatables.js') }}"></script>
<script src="{{ asset('utils/bootbox/bootbox.js') }}"></script>
<script src="{{ asset('utils/sweetalert2/dist/sweetalert2.js') }}"></script>
<script src="{{ asset('utils/datepicker/datepicker.js') }}"></script>
{{ $script }}
<script>
  $('#cta-logout').click(() => {
    Swal.fire({
      title: 'Yakin mau keluar sekarang?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Keluar',
      cancelButtonText: 'Belum',
    }).then((response) => {
      if(response.isConfirmed){
        $('#logout').click();
      }
    });
  });
</script>
</body>
</html>

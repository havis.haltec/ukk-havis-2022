<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? env('APP_NAME') }}</title>
    <link rel="stylesheet" href="{{ asset('utils/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/datatable/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/font-awsome-6/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/sweetalert2/dist/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">
    <link rel="icon" href="{{ asset('favicon/favicon.ico') }}" type="image/x-icon" />

</head>
<body class="body-img">
    <div class="container">
        <div class="row py-3">
            <span class="text-white text-logo">HOTEL HEBAT</span>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 bg-white rounded vh-50 shadow">
                <div class="row py-2 bg-danger">
                    <span class="text-white text-bold tracking-wider">Lagi Promo !!!</span>
                </div>
                <div class="container mt-3">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('utils/jquery/jquery-3.6.0.js') }}"></script>
    <script src="{{ asset('utils/datatable/datatables.js') }}"></script>
    <script src="{{ asset('utils/font-awsome-6/js/all.js') }}"></script>
    <script src="{{ asset('utils/sweetalert2/dist/sweetalert2.js') }}"></script>
    <script src="{{ asset('utils/bootstrap/js/bootstrap.js') }}"></script>
</body>
</html>
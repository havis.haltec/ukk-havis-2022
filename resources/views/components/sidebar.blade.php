<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if(auth()->user()->role == 'admin')
          <li class="nav-item">
            <a href="{{ route('kamar.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Kamar
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('fasilitas.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Fasilitas Hotel
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('fasilitas-kamar.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Fasilitas kamar
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('tipe-kamar.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Tipe kamar
              </p>
            </a>
          </li>
        @else
          <li class="nav-item">
            <a href="{{ route('pendapatan.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Pendapatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('buku.tamu.index') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Resepsionis
              </p>
            </a>
          </li>
        @endif
      </ul>

      <div class="sidebar-custom">
        {!! Form::open(['route' => 'logout', 'method' => 'post']) !!}
            <button type="button" class="btn btn-danger w-100 py-1 px-2" id="cta-logout"><i class="fa-solid fa-right-from-bracket mr-2"></i><b>Keluar</b></button>
            <button class="btn btn-danger w-100 py-1 px-2 d-none" id="logout"><i class="fa-solid fa-right-from-bracket mr-2"></i><b>Keluar</b></button>
        {!! Form::close() !!}
      </div>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
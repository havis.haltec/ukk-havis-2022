<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? env('APP_NAME') }}</title>
    <link rel="stylesheet" href="{{ asset('utils/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/datatable/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/font-awsome-6/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/sweetalert2/dist/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('utils/datepicker/datepicker.css') }}">
    <link rel="icon" href="{{ asset('favicon/favicon.ico') }}" type="image/x-icon" />
</head>
<body class="body">
    {{-- Navbar --}}
    <nav class="container-fluid bg-white p-4 shadow sticky-top">
        <div class="row align-items-center">
            <div class="col-md-4">
                <span class="text-dark fw-bold fs-5">HOTEL HEBAT</span>
            </div>
            <div class="col-md-8 text-end">
                <div class="d-inline m-3">
                    <a href="{{ route('landing') }}" class="text-link m-2 {{ Route::current()->getName() == "landing" ? "text-danger" : "text-dark"}}">Home</a>
                    <a href="{{ route('fasilitas.hotel') }}" class="text-link m-2 {{ Route::current()->getName() == "fasilitas.hotel" ? "text-danger" : "text-dark"}}">Fasilitas</a>
                    <a href="{{ route('kamar.hotel') }}" class="text-link m-2 {{ Route::current()->getName() == "kamar.hotel" ? "text-danger" : "text-dark"}}">Kamar</a>
                </div>
                @if(!auth()->user())
                    <div class="d-inline m-3">
                        <a href="{{ route('login') }}" class="btn btn-outline-primary">Login</a>
                    </div>
                    <a class="btn btn-danger fw-bold" href="{{ route('register') }}">Daftar</a>
                @else
                    <div class="d-inline">
                        <span class="text-dark">Hi, {{ auth()->user()->name }}</span>
                    </div>
                    <div class="d-inline m-3">
                        @if( App\Models\Cart::activeUser()->count() > 0)
                            <a href="{{ route('cart.index') }}" class="btn btn-outline-primary"><i class="fa fa-shopping-cart"></i> Keranjang <span id="count" class="fw-bold">{{ App\Models\Cart::activeUser()->count() }}</span></a>
                        @else
                            <a href="{{ route('cart.index') }}" class="btn btn-outline-primary"><i class="fa fa-shopping-cart"></i> Keranjang <span id="count" class="fw-bold"></span></a>
                        @endif
                    </div>
                    {!! Form::open(['route' => 'logout', 'method' => 'post', 'class' => 'd-inline']) !!}
                        <button class="btn btn-danger fw-bold"><i class="fa-solid fa-right-from-bracket"></i> Keluar</a>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </nav>
    <div class="container mt-4">
        {{ $slot }}
    </div>
    <footer class="container-fluid bg-white p-4 border-top border-5 border-danger">
        <div class="row align-items-center text-center">
            <span class="fw-light fs-5 text-opacity-gray">copyright @ hotel hebat</span>
        </div>
    </footer>
    <script src="{{ asset('utils/jquery/jquery-3.6.0.js') }}"></script>
    <script src="{{ asset('utils/datatable/datatables.js') }}"></script>
    <script src="{{ asset('utils/font-awsome-6/js/all.js') }}"></script>
    <script src="{{ asset('utils/sweetalert2/dist/sweetalert2.js') }}"></script>
    <script src="{{ asset('utils/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('utils/bootbox/bootbox.js') }}"></script>
    <script src="{{ asset('utils/datepicker/datepicker.js') }}"></script>
    <script>
        setInterval(() => {
            $.ajax({
                url:'{{ route('cart.destroy.all') }}',
                method: 'post',
                data: { '_method': 'post', '_token': '{{ csrf_token() }}' },
                success:(response) => {
                    if(response.status){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Keranjang dihapus',
                            text: response.message
                        })
                    }
                }
            })
        }, 500000);
    </script>
    {{ $script ?? '' }}
</body>
</html>